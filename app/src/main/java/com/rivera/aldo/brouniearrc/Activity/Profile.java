package com.rivera.aldo.brouniearrc.Activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rivera.aldo.brouniearrc.Adapters.CustomListProject;
import com.rivera.aldo.brouniearrc.Adapters.CustomListWork;
import com.rivera.aldo.brouniearrc.DataBase.SqliteHelper;
import com.rivera.aldo.brouniearrc.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Profile extends AppCompatActivity {

    String nameGot,idGot,imageGot,phoneGot,path;
    TextView txtName,txtPhone;
    ImageView profile,photo;
    ArrayList<String> workTeams;
    CustomListProject adapter;
    GridView grid;
    Uri imageUri;
    private static final int PICK_IMAGE=100;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        txtName = (TextView)findViewById(R.id.txtName);
        txtPhone = (TextView)findViewById(R.id.txtPhone);
        profile = (ImageView)findViewById(R.id.profileImage);
        grid = (GridView) findViewById(R.id.grid);

        idGot = getIntent().getStringExtra("id");
        nameGot = getIntent().getStringExtra("name");
        imageGot = getIntent().getStringExtra("image");
        phoneGot = getIntent().getStringExtra("phone");

        txtName.setText("NAME: "+nameGot.toUpperCase());
        txtPhone.setText("PHONE: "+phoneGot);

        dataGrid();

        if(!checkAndRequestPermissions()){
            checkAndRequestPermissions();
        }

        File imgFile = new  File(imageGot);
        System.out.println(imageGot);
        if(imgFile.exists()){
            profile.setImageDrawable(Drawable.createFromPath(imageGot));
        }

    }

    private void dataGrid() {

        SqliteHelper helper = new SqliteHelper(Profile.this, "Brou",null,1);
        workTeams = helper.showWorkersProject(helper,Integer.parseInt(idGot));
        adapter = new CustomListProject(Profile.this, workTeams);
        grid.setAdapter(adapter);

    }

    public void openMap(View view) {
        Uri gmmIntentUri = Uri.parse("https://www.google.com.mx/maps/place/Brounie/@20.6124531,-100.3880801,17z/data=!3m1!4b1!4m5!3m4!1s0x85d35b18b0861e2b:0x2a6ac0adc40987ac!8m2!3d20.6124481!4d-100.3858914");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    public void editProfile(View view) {

        Intent i = new Intent(Profile.this,Cuatro.class);
        i.putExtra("id",idGot);
        i.putExtra("name",nameGot);
        i.putExtra("image",imageGot);
        i.putExtra("phone",phoneGot);
        startActivity(i);
        finish();

    }

    public void deleteWorker(View view) {
        mostrarDialogo();
    }

    private void mostrarDialogo() {

            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle("¡Hey!")
                    .setMessage("Do you want to delete it?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SqliteHelper helper = new SqliteHelper(Profile.this,"Brou",null,1);
                            helper.deleteW(Profile.this,helper,Integer.parseInt(idGot));
                            startActivity(new Intent(Profile.this,Dos.class));
                            finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .create();
            dialog.show();
    }

    public void addProject(View view) {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(Profile.this);
        View view1 = getLayoutInflater().inflate(R.layout.project,null);
        builder.setView(view1);
        Button btnAddProject = (Button)view1.findViewById(R.id.btnAddProject);
        final EditText txtNameProject = (EditText) view1.findViewById(R.id.txtNameProject);
        photo = (ImageView)view1.findViewById(R.id.photo);
        final android.app.AlertDialog dia = builder.create();
        dia.show();
        builder.setCancelable(true);
        btnAddProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String projectName = txtNameProject.getText().toString();
                if(projectName.equals("") || path.equals("")){
                    Toast.makeText(Profile.this, "You missed something", Toast.LENGTH_SHORT).show();
                }else{
                    saveProject(projectName,path,idGot);
                }
            }
        });

    }

    private  boolean checkAndRequestPermissions() {
        int permissionRead = ContextCompat.checkSelfPermission(this,  Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionCamara = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();


        if (permissionRead != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (permissionWrite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionCamara != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK  && requestCode == PICK_IMAGE){
            imageUri = data.getData();
            Cursor cursor = getContentResolver().query(imageUri, null, null, null, null);
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            path = cursor.getString(idx);
            photo.setImageDrawable(Drawable.createFromPath(cursor.getString(idx)));
        }

    }

    public void backToList(View view) {
        finish();
    }

    public boolean chooseImage(View view) {
        Intent galeria = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(galeria, PICK_IMAGE);
        return true;
    }

    public void saveProject(String name, String path, String id) {
        try{
            System.out.println("PATH: "+ path);
            SqliteHelper helper = new SqliteHelper(Profile.this,"Brou",null,1);
            helper.insertProject(name,path,Profile.this,helper,Integer.parseInt(idGot));
            startActivity(new Intent(Profile.this,Uno.class));
            finish();

        }catch (Exception e1){
            System.out.println("----------------------:"+e1.getMessage()+":-------------------------");
        }

    }

}
