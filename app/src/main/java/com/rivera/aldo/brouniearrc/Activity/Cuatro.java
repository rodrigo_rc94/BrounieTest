package com.rivera.aldo.brouniearrc.Activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rivera.aldo.brouniearrc.DataBase.SqliteHelper;
import com.rivera.aldo.brouniearrc.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Cuatro extends AppCompatActivity {
    String nameGot,idGot,imageGot,phoneGot,path;
    Uri imageUri;
    EditText txtName,txtPhone;
    Button save;
    ImageView profile;

    private static final int PICK_IMAGE=100;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuatro);

        txtName = (EditText) findViewById(R.id.txtName);
        txtPhone = (EditText) findViewById(R.id.txtPhone);
        profile = (ImageView)findViewById(R.id.profileImage);
        save = (Button) findViewById(R.id.btnSave);

        getSupportActionBar().hide();

        idGot = getIntent().getStringExtra("id");
        nameGot = getIntent().getStringExtra("name");
        imageGot = getIntent().getStringExtra("image");
        phoneGot = getIntent().getStringExtra("phone");
        path = imageGot;

        if(!checkAndRequestPermissions()){
            checkAndRequestPermissions();
        }

        txtName.setText(nameGot);
        txtPhone.setText(phoneGot);
        File imgFile = new  File(imageGot);
        System.out.println(imageGot);
        if(imgFile.exists()){
            profile.setImageDrawable(Drawable.createFromPath(imageGot));
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK  && requestCode == PICK_IMAGE){
            imageUri = data.getData();
            Cursor cursor = getContentResolver().query(imageUri, null, null, null, null);
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            path = cursor.getString(idx);
            profile.setImageDrawable(Drawable.createFromPath(cursor.getString(idx)));
        }

    }

    public void updateData() {
        String nameUp = txtName.getText().toString();
        String phoneUp = txtPhone.getText().toString();
        try{
            System.out.println("PATH: "+path);
            if(path.equals("")){
                Toast.makeText(this, "We need the image, please select one.", Toast.LENGTH_SHORT).show();
            }else{
                if(nameUp.equals("") || phoneUp.equals("")){
                    Toast.makeText(this, "You forgot something.", Toast.LENGTH_SHORT).show();
                }else{
                    UpdateStorage(nameUp,path,phoneUp,idGot);
                }
            }

        }catch (Exception e1){
            System.out.println("----------------------:"+e1.getMessage()+":-------------------------");
        }

    }

    private void UpdateStorage(String nameUp, String phoneUp, String imageUri, String idGot) {
        SqliteHelper helper = new SqliteHelper(Cuatro.this,"Brou",null,1);
        helper.updateW(nameUp,phoneUp,imageUri,Cuatro.this,helper,Integer.parseInt(idGot));
        startActivity(new Intent(Cuatro.this,Uno.class));
        finish();
    }

    public boolean selectPhoto(View view) {
            Intent galeria = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            startActivityForResult(galeria, PICK_IMAGE);
        return true;
    }

    private  boolean checkAndRequestPermissions() {
        int permissionRead = ContextCompat.checkSelfPermission(this,  Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionCamara = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();


        if (permissionRead != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (permissionWrite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionCamara != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    public void updateDataStore(View view) {
        updateData();
    }
}
