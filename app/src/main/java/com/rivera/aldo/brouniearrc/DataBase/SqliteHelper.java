package com.rivera.aldo.brouniearrc.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.rivera.aldo.brouniearrc.Activity.Profile;

import java.util.ArrayList;

/**
 * Created by Aldo on 11/09/2017.
 */

public class SqliteHelper extends SQLiteOpenHelper {

    String query = "CREATE TABLE work (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "name TEXT," +
            "image TEXT," +
            "phone TEXT," +
            "category Text" +
            ");";
    String query2 ="CREATE TABLE project (" +
            "idProject INTEGER PRIMARY KEY AUTOINCREMENT," +
            "name TEXT," +
            "image TEXT," +
            "idWorker INTEGER," +
            "FOREIGN KEY (idWorker) REFERENCES work(id)" +
            ");";

    public SqliteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(query);
        db.execSQL(query2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query = "DROP TABLE projects";
        db.execSQL(query);
        String querys = "DROP TABLE worker";
        db.execSQL(querys);
    }

    public void insertW(String name, Context con, SqliteHelper helper, String namePass){
        ContentValues c = new ContentValues();
        SQLiteDatabase db = helper.getWritableDatabase();
        c.put("name",name);
        c.put("image","no");
        c.put("phone","442.###.####");
        c.put("category",namePass);
        db.insert("work",null,c);
        Toast.makeText(con, "Saved", Toast.LENGTH_SHORT).show();
    }

    public void updateW(String name,String img,String phone, Context con, SqliteHelper helper, int id){
        String query = "UPDATE work set name ='"+name+"', image ='"+img+"', phone='"+phone+"' WHERE id ="+id;
        System.out.println(query);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.execSQL(query);
        Toast.makeText(con, "Updated", Toast.LENGTH_SHORT).show();
    }

    public void deleteW(Context c, SqliteHelper helper, int id){
        String query = "DELETE FROM work WHERE id = "+id;
        String query2 = "DELETE FROM project WHERE idWorker = "+id;
        SQLiteDatabase db = helper.getWritableDatabase();
        db.execSQL(query2);
        db.execSQL(query);
        Toast.makeText(c, "Deleted", Toast.LENGTH_SHORT).show();
    }

    public ArrayList<String> showWorkers(SqliteHelper helper, String catergory){
        String query = "SELECT * FROM work WHERE category = '"+catergory+"'";
        ArrayList<String> ob = new ArrayList<>();
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery(query,null);
        if(c.moveToFirst()){
            do {
                String row = c.getInt(0)+"-"
                            +c.getString(1)+"-"
                            +c.getString(2)+"-"
                            +c.getString(3)+"-"
                            +c.getString(4)+"-";
                ob.add(row);
            }while (c.moveToNext());
        }
        return ob;
    }

    public  ArrayList<String>  showWorkersProject(SqliteHelper helper, int id){
        String query = "SELECT * FROM project WHERE idWorker = "+id;
        ArrayList<String> ob = new ArrayList<>();
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery(query,null);
        if(c.moveToFirst()){
            do {
                String row = c.getInt(0)+"-"
                        +c.getString(1)+"-"
                        +c.getString(2)+"-";
                ob.add(row);
            }while (c.moveToNext());
        }
        return ob;
    }

    public void insertProject(String name, String path, Context con, SqliteHelper helper, int id) {
        ContentValues c = new ContentValues();
        SQLiteDatabase db = helper.getWritableDatabase();
        c.put("name",name);
        c.put("image",path);
        c.put("idWorker",id);
        db.insert("project",null,c);
        Toast.makeText(con, "Saved", Toast.LENGTH_SHORT).show();
    }
}
